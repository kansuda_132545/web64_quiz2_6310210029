const { query } = require('express');
const mysql = require('mysql')
const { CLIENT_LONG_PASSWORD } = require('mysql/lib/protocol/constants/client')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'admin',
    password : '1234' ,
    database : 'rose-hotel'
});

connection.connect();

const express = require('express');
const app = express()
const port = 4000


/*Middleware for Authenticating User Token */
function authenticateToken(req ,res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)

    jwt.verify(token, TOKEN_SECRET, (err, user) =>{
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

/* API for Processing Hotel Authorization */
app.post("/login", (req, res) =>{
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM RoseHotel WHERE Username = '${username}'`
    connection.query( query, (err,rows) =>{
                if (err) {
                    console.log(err)
                    res.json({
                        "status" : "400",
                        "message" : "Error querying from running db"
                        })
                }else {
                    let db_password = rows[0].Password
                    bcrypt.compare(user_password, db_password, (err, result) =>{
                        if (result){
                           let payload = {
                               "username" : rows[0].Username,
                               "user_id" : rows[0].HotelID,
                           }

                           let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d' } )
                           res.send(token)
                        }else { res.send ("Invalid username / password")}
                    })
                

                } 
    })
})/*API for Registering a new Runner */
app.post("/register_hotel", (req,res) =>{
    let id  = req.query.id
    let name = req.query.name
    let age= req.query.age
    let member_id = req.query.member_id
    
    bcrypt.hash(SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO  Hotel 
                 (Name, Id , Age , Member_ID )
                 VALUES ('${name}','${id}','${age}', '${member_id}')`
        
        console.log(query)
        connection.query( query,(err, rows) =>{
                if (err) {
                    res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                        })
                }else {
                    res.json ({
                            "status" : "200",
                            "message" : "Adding new user succesful"
                    })
            }
        });

    })
         
 })
 /* CRUD  Operation for RoseHotel Table */
app.get("/list_hotel",authenticateToken , (req, res) => {
    query = "SELECT * from RoseHotel";
connection.query( query, (err, rows) => {
        if (err) {
                res.json({
                            "status" : "400",
                            "message" : "Error querying from running db"
                        })
        }else {
            res.json(rows)
        }
    });
})

app.post("/update_hotel",authenticateToken,(req,res) =>{
    let hotel_id = req.query.hotel_id
    let hotel_name  = req.query.hotel_name
    let hotel_age = req.query.hotel_age

    let query = `UPDATE Hotel SET 
                 HotelAge = '${hotel_age}'
                 HotelName = '${hotel_name}',
                 WHERE HotelID  = ${hotel_id}`

    console.log(query)

    connection.query( query,(err, rows) =>{
        if (err) {
            console.log(err)
            res.json({
                   "status" : "400",
                   "message" : "Error updating record"
            })
        }else {
            res.json ({
                    "status" : "200",
                    "message" : "Updating hotel succesful"
            })
        }
    });
})


app.post("/add_hotel", (req, res)  =>  {


    let hotel_id = req.query.hotel_id
    let hotel_type = req.query.hotel_type
    let hotel_price = req.query.hotel_price

    let query = `INSERT INTO RoseHotel 
    (HotelID, HotelType,HotelPrice)
                VALUES ('${hotel_id}','${hotel_type}','${hotel_price}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding hotel succesful"
            })
        }
    });
    
})


app.post("/delete_hotel", (req, res)  =>  {

    let hotel_id = req.query.id
   

    let query = `DELETE FROM hotel WHERE HotelID=${hotel_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error deleting record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deleting record succesful"

            })
            
        }
    });

})


app.listen(port, () => {
    console.log(`Now starting Running System Backend ${port} `)
})
